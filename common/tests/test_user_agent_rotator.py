from scraper.user_agent_rotator import UserAgentRotator

import unittest


class TestUserAgentRotator(unittest.TestCase):

    def test_get_user_agent(self):
        uar = UserAgentRotator()
        res = uar.get_user_agent()
        self.assertIsInstance(res, str, "random user choice is string")
        self.assertGreater(len(res), 20, "string has something")

    def test_generate_header(self):
        uar = UserAgentRotator()
        res = uar.generate_header()
        self.assertEqual(res["Connection"], "close",
                         "generate header with connection closed")
        self.assertGreater(len(res["User-Agent"]), 20,
                           "string has something")


if __name__ == "__main__":
    unittest.main()
