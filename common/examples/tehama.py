import re
from bs4 import BeautifulSoup
from scraper import Scraper

# we initialise a bot tahema that will have run the parsing functions
tahema = Scraper("tahema", start_date="12/1/2016", end_date="12/1/2016")

domain = "http://tehamapublic.countyrecords.com"
context_root1 = "scripts/hfweb.asp?formuser=public&Application=TEH"
context_root2 = "scripts/hfweb.asp"
context_root3 = "scripts/hflook.asp"

# we put the parameters that a domain will need to land to the next page along
# with the page details
tahema_args = {
    "page1": {
        "url": "{domain}/{context_root}".format(domain=domain,
                                                context_root=context_root1),
        "params": None
    },
    "page2": {
        "url": "{domain}/{context_root}".format(domain=domain,
                                                context_root=context_root2),
        "params": {"Database": "TEHOR",
                   "x": "16",
                   "y": "16",
                   "FormUser": "public",
                   "APPLICATION": "TEH"}
    },
    "page3": {
        "url": "{domain}/{context_root}".format(domain=domain,
                                                context_root=context_root3),
        "params": {
            "APPLICATION": "TEH",
            "DATABASE": "OR",
            "BASKET": "",
            "databasename": "Official+Records+thru+02/16/2017",
            "SEARCHTYPE7": "exact",
            "FIELD7": "",
            "FIELD8": "",
            "SEARCHTYPE8": "EXACT",
            "FIELD9": "12/1/2016",
            "FIELD9B": "12/1/2016",
            "SEARCHTYPE11": "begin",
            "FIELD11": "",
            "SEARCHTYPE12": "begin",
            "FIELD12": "",
            "CROSSNAMETYPE": "begin",
            "CROSSNAMEFIELD": "",
            "FIELD15": "",
            "SEARCHTYPE16": "exact",
            "FIELD16": "",
            "SEARCHTYPE17": "exact",
            "FIELD17": "",
            "FIELD25": "",
            "FIELD25B": "",
            "DataAction": "Search"
        }
    }
}

# below write the parsing functions for the data in the webpages for each web
# page
@tahema.scrape(tahema_args["page1"]["url"])
def parse_page1():
    # we dont need to make any parsing done in this page and hence we will just
    # pass here and not have any return statement
    pass


@tahema.scrape(tahema_args["page2"]["url"],
               params=tahema_args["page2"]["params"],
               cookies=tahema.response.cookies)
def parse_page2():
    # here also we just pass the date parameter
    # so no parsing
    pass


def get_cells_data(cells):
    def cell_data(cells, indx):
        try:
            txt = cells[indx][0]
            return txt
        except IndexError:
            return ""

    return (cell_data(cells, i) for i in range(2, 11))


def parse_view_page(url):
    with Scraper("bot", start_date="12/1/2016", end_date="12/1/2016") as s:
        response = s.requests_response(url)
        soup = BeautifulSoup(response.text, "html.parser")
        for body_row in soup.select("table tr"):
            cells = body_row.findAll('td')

            for cell in cells:
                print(cells)
                import time
                time.sleep(2)


def handle_deed_elem(rec, domain):
    view_elem = rec[1][0]
    view_elem = repr(view_elem)
    p = re.compile("href=.*\>")
    match = p.search(view_elem)
    link = match.group(0)
    link = link.replace("href=\"", "")
    link = link.replace("\"><b>VIEW</b></a>", "")
    url = "/".join([domain, link])
    parse_view_page(url)



@tahema.scrape(tahema_args["page3"]["url"],
               params=tahema_args["page3"]["params"],
               cookies=tahema.response.cookies)
def parse_page3():
    # We have the source of the page, let's ask BeaultifulSoup to parse it for
    # the bot
    soup = BeautifulSoup(tahema.response.text, "html.parser")
    # to do: write the remaining of the functions
    # look into https://gist.github.com/phillipsm/0ed98b2585f0ada5a769
    for body_row in soup.select("table tr"):
        cells = body_row.findAll('td')


        for cell in cells:
            table_row = cell.findAll('td')

            if len(table_row) > 5:

                element_contents = [element.contents for element in table_row]

                def remove_irrelavant(element_contents):
                    for x in element_contents:
                        if "NumberOfPages" in repr(x):
                            return element_contents.index(x)

                try:
                    remove_before = remove_irrelavant(element_contents) + 1
                except TypeError:
                    remove_before = 0

                element_contents = element_contents[remove_before:]

                element_contents_length = len(element_contents)
                step = 11
                page_records = [element_contents[i:i + step] for i in range(0, element_contents_length, step)]

                # for rec in page_records[1:]:
                for rec in page_records[1:]:
                    if not "<b>" in repr(rec[0][0]):
                        # here the elements are marked to the respective csv
                        # headers. This will be used by the csvwriter to write
                        # the corresponding value under the correct column
                        elem = dict(zip(tahema.csvheaders, get_cells_data(rec)))
                        yield elem

                        # deed_elem = handle_deed_elem(rec, domain)
                        # if deed_elem:
                        #     yield deed_elem


# put the functions in a list where the sequence should be in the order that you
# want the bot to crawl through the pages
parse_functions = [parse_page1, parse_page2, parse_page3]
# define the headers for the csv file that will be generated
tahema.csvheaders = ["Instrument_Number",
                  "InstrumentType",
                  "SUBSTITUTION OF TRUSTEE",
                  "Recording_Date",
                  "Grantor",
                  "Grantee",
                  "Comments",
                  "Book",
                  "Page",
                  "NumberOfPages"]


if __name__ == "__main__":
    tahema.run(parse_functions)
