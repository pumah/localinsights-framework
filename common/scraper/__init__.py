# -*- coding: utf-8 -*-
import requests
import time

try:
    # test if BeautifulSoup is installed and the user wants to parse using css
    from bs4 import BeautifulSoup
    from bs4 import FeatureNotFound
except ImportError:
    pass

try:
    # test if only lxml is there
    from lxml import html
except NameError:
    pass

from scraper.utils import save_to_csv
from scraper.utils import setup_logging
from scraper.utils import convert_to_filenameable
from scraper.utils import create_csv
from scraper.utils import push_to_ftp
from scraper.utils import add_identifiers
from scraper.utils import is_production_server
from scraper.user_agent_rotator import UserAgentRotator
from scraper.proxy_rotator import ProxyRotator
from scraper.exceptions import ScraperError
from scraper.call_to_proxy_api import TaskHandler
import json

"""
Base class for bot framework scrapers

Wiki: https://bitbucket.org/lastlook1/localinsights-framework/wiki/Home
Issue tracker: https://bitbucket.org/lastlook1/localinsights-framework/issues?status=new&status=open
Some examples: https://bitbucket.org/lastlook1/localinsights-framework/src/285d34c356861d22e1d078b2eb98d984c22933ec/common/examples/?at=master

To DO:
    1. rotate through different user agents
    2. a simple command to create boilerplate code

thoughts:
    make the CLI such that based on the dates it should start separate workers
    of the whole engine based on individual dates. One by one it will complete
    the dates. The multiprocessing will be done at the higher level from
    from this script
"""

__version__ = '0.2.1'

DELAY = 1
scraper_logging = "stdout"  # the other is "file"

# the following dependency can be changed to selenium in case you want to parse
# the whole thing using selenium.
# so effecitvely you can write::
#    import scraper
#    scraper.REQUEST_DEPENDENCY = 'selenium'
REQUEST_DEPENDENCY = 'requests'


class Scraper:
    """
    Base class for scrapers. All bots must inherit from this class

    Parameters:
    -----------
    start_date: str
        string will be a date in mm/dd/yyyy format
    end_date: str
        a valid date in mm/dd/yyyy format
    headers: List[str]
        a list of headers that should be in the csv file
    response: requests.response
        a response object. This is kept so that the previous response can be
        used in the current request as well
    delay: int
        number of seconds that you want the crawler to be delayed by
        This is so that the bot is not flagged and blacklisted.

    Examples:
    ---------
        You can use it as a simple class

        >>> mybot = Scraper("mybot", start_date="2/1/2017", end_date="2/1/2017")

        Or as a context manager

        >>> with Scraper("mybot",
        ...              start_date="2/1/2017",
        ...              end_date="2/1/2017") as mybot:
        ...     # write the code here

    """
    # we will call the task handler which will make the call to the proxy
    # handler which will call the threads that make the connection to the
    # api and save the proxies to a file. we will call the closing of the
    # apis at the end which should give a good amount of time for the
    # threads to close by themselves.
    th = TaskHandler()
    if is_production_server():
        th.start_the_tasks()

    def __enter__(self):
        """the enter method for the context manager"""
        return self

    def __exit__(self, *_args):
        """the exit method for the context manager"""
        pass

    def __repr__(self):
        return '%s(%r)' % (self.__class__.__name__, self.name)


    def __init__(self, name,filename=None, start_date=None, end_date=None,
                 csvheaders=None, csvfile = None, response=None, delay=DELAY,
                 log=None, scraper_proxies=None, bot_last_status=None,
                 *args, **kwargs):
        # the name of the package is used to resolve resources from inside the
        # package or the folder the module is contained in the resolve.
        self.name = name

        # start date and end date is needed so that the parser is able to have a
        # set time limit within which the bot will parse the data in the pages.
        # This is also needed for giving the determining the name of the csv
        # files and the log files where the respective information will go.
        self.start_date = start_date
        self.end_date = end_date

        # The class that is used for response objects. In the present scenario
        # this is a response object of the requests class.
        self.response = response

        # an instance will have a default proxy that
        # will be fixed for the particular instance and will act as a random
        # agent for the bot
        self._scraper_proxies = scraper_proxies

        # log handler is set, where the file name is based on the starting date
        # and the name of the bot as given by the user. if the logging flag is
        # set to stdout, log file is not initialised and the stdout is to
        # stdout.
        log_date = None
        if self.start_date:
            log_date = convert_to_filenameable(self.start_date)

        '''
        there are two cases here:
        1. filename is passed to call save
        2. filename is not passed as save isn't being called in this object

        for case 1, we need both csvname and logfile name, hence the check
        for create_csv ( create a csv only when we definitely want to call save

        for case 2, we need only logfile name
        '''
        if not filename:
            self.filename = name
        else:
            self.filename = filename

        if log_date:
            self.filename = add_identifiers(self.filename, log_date)

        # csvheaders are the headers that should be there in the csv file and
        # will be defined by the use
        self.csvheaders = csvheaders

        # define the csv file where the data should be placed. If the csv file
        # is supplied by the user then use that. Else the csv file will be
        # generated from the filename that is given.
        if csvfile:
            self.csvfile = csvfile
        else:
            if self.filename is not None:
                self.csvfile = create_csv(self.filename)
            else:
                self.csvfile = self.filename

        if scraper_logging == "stdout":
            log_file = None
        elif scraper_logging == "file":
            log_file = "log/{filename}.log".format(filename=self.filename)
        else:
            raise ScraperError("please set correct logging flag for"
                               "scraper_logging")

        # set up logging
        # you can also setup custom logging
        # this will go to stdout in case log_file is not there
        # the logging will be done on stdout
        if log:
            self.log = log
        else:
            self.log = setup_logging(log_file)

        # a delay of `DELAY` seconds will be set for the bot. This is 1 second
        # by default. You can change this by changing the delay parameter.
        self.delay = delay

        # will have a definition for bot last status to keep a track of the last
        # status of the bot. This will be a dictionary
        self.bot_last_status = bot_last_status

        # dependency injection for requests
        # if the selenium is used as a dependency then we initialise a selenium
        # webdriver
        if REQUEST_DEPENDENCY == 'requests':
            self.requests = requests
        elif REQUEST_DEPENDENCY == 'selenium':
            try:
                from selenium import webdriver
                from selenium.webdriver import DesiredCapabilities
            except (NameError, ModuleNotFoundError):
                raise ScraperError('You are using selenium without installing it.\n'
                    'Please install it\n'
                    'pip install selenium\n')
            desired_capabilities = DesiredCapabilities.PHANTOMJS.copy()
            desired_capabilities['phantomjs.page.customHeaders.User-Agent'] = self.headers

            # to do work on the proxy system as well after the proxy PR is
            # merged
            # reference https://github.com/ariya/phantomjs/issues/11619
            # desiredCapabilities['proxy'] = {
            #     proxyType: 'manual',
            #     httpProxy: '10.0.0.1:8080',
            #     httpsProxy: '10.0.0.1:8080',
            # }
            if 'executable_path' in kwargs:
                self.requests = webdriver.PhantomJS(executable_path=kwargs.get('executable_path'),
                                                    desired_capabilities=desired_capabilities)

    @property
    def scraper_proxies(self):
        '''get the scraper_proxies'''
        if not self._scraper_proxies and is_production_server():
                return ProxyRotator().proxies
        else:
            return self._scraper_proxies

    @scraper_proxies.setter
    def scraper_proxies(self, scraper_proxies):
        self._scraper_proxies = scraper_proxies

    @property
    def headers(self):
        '''get the headers'''
        # This will need to be defined by the user, as he would have already
        # seen the page and would have a clear picture about the headers that
        # should be there in the csv file
        return UserAgentRotator().generate_header()

    def scrape(self, url, *args, **kwargs):
        """
        A decorator that is used to register a scrape function for a
        given URL.

             @scraper.scrape(url, params)
             def parse_page():
                 data = scraper.response.text
                 return data

        Parameters:
        -----------
        url: str
            the URL rule as string
        params: dict(str: str)
            a dict object containing the parameters to be given to requests for
            the next post request.
        """
        def decorator(f):
            # we will make use of the request lib here
            # this is taken a design choice as the urls we want to use are more
            # of server side rendering with less or almost no js
            self.response = self.requests_response(url, *args, **kwargs)
            return f
        return decorator

    def requests_response(self, url, *args, **kwargs):
        """Returns the response object. It will either do a post based on
        whether there are params and cookies passed or not, or will do a simple
        get request. This is the implementation of scrape as a method.

        :param url: the URL rule as string
        :param params: a dict object containing the parameters to be given to
                       requests for the next post request.

        """
        log_first_part = "sending request to {url}".format(url=url)
        if args or kwargs:
            for_logging = {'args': repr(args), 'kwargs': repr(kwargs)}
            log_sec_part = "with {args} and {kwargs}".format(**for_logging)
            # self.log.info(" ".join([log_first_part, log_sec_part]))
            time.sleep(self.delay)
            return self.requests.post(url, *args,
                                      headers=self.headers,
                                      proxies=self.scraper_proxies,
                                      **kwargs)
        else:
            # self.log.info(log_first_part)
            time.sleep(self.delay)
            return self.requests.get(url, headers=self.headers,
                                     proxies=self.scraper_proxies)

    def selenium_get(self, url):
        '''Do a GET to the webpage

        :param url: The url as a string object'''
        if REQUEST_DEPENDENCY == 'selenium':
            self.requests.get(url)

    def _save_records_to_csv(self, records):
        """
        This will take the records and save it to a csv file

        Parameters:
        -----------'
        records: List[dict]

        Returns:
        -------
        path: str
            This will return the path of the csv file where the records has been
            saved
        """
        if records:
            try:
                path = save_to_csv(self.csvheaders, self.csvfile, records)
                # this is a function that has a side effect
                # that is it stores the records to a file
                # so we return a json to say that the file is saved and the path
                # is this. Also additional information are given like the
                # headers and the date. This is mainly to save the information
                # to a log file later we will assert to check if this is the
                # same csv file was used to write the data
                assert self.csvfile == path

                # self.log.info("csv file {path}".format(path=path))
                return path
            except TypeError:
                information = "cannot save to csv as start date is not set"
                self.log.debug(information)
                raise ScraperError(information)
            except AttributeError:
                p = "the return value of the functions has to be an iterable"
                self.log.debug(p)
                raise ScraperError(p)

    def _execute(self, fn):
        try:
            records = fn()
            return records
        except AttributeError:
            # this has to be laced with meaningful exceptions
            information = " ".join(["the function ",
                                    "{fn_name}".format(fn_name=fn.__name__),
                                    "has a return value that is wrong.",
                                    "It should be dict ",
                                    "please check"])
            self.log.debug(information)
            raise ScraperError(information)

    def save(self, records, path="~"):
        """this is the function used to save bulk data in the csv"""
        records = [x for x in records if x is not None]

        for record in records:
            self._save_records_to_csv(record)

        # if self.name:
        #     path = "/localinsight/csv/county_recorder/"

        push_to_ftp(self.csvfile)
        print(json.dumps({"status": self.bot_last_status}))
        # self.log.info(self.bot_last_status)

        # we will call the closing of the api calling threads now. This is done
        # here in the assumption that the save function is the ending function
        # that is called by the scripts
        self.th.finishing_the_tasks()

    def run(self, fn_list):
        """Run the parsing functions that are defined by the user

        Parameters:
        -----------
        fn_list: List[function]
            a list of functions. The sequence will be in which the user wants to
            execute it.

        Returns:
        --------
        return_values: None

        Examples:
        ---------
        >>> fn_list = [parse1, parse2, parse3]
        >>> mybot.run(fn_list)
        """
        records = map(self._execute, fn_list)

        # remove the null values, clean the data
        records = [x for x in records if x is not None]

        for record in records:
            self._save_records_to_csv(list(record))

        self.bot_last_status = {"path": self.csvfile, "date": self.start_date,
                                "headers": self.csvheaders}
        self.log.info(self.bot_last_status)

    def find_element_by_xpath(self, xp):
        """
        Finds an element by xpath.

        :Args:
         - xp: The xpath to use when finding elements.

        :Usage:
            scraper_object.find_element_by_xpath('//div/td[1]')
        """
        for elem in self.tree.xpath(xp):
            return elem

    def find_elements_by_xpath(self, xp):
        '''
        Finds multiple elements by xpath.

        :Args:
         - xpath - The xpath locator of the elements to be found.

        :Usage:
            scraper_object.find_elements_by_xpath("//div[contains(@class, 'foo')]")
        '''
        return self.tree.xpath(xp)

    @property
    def soup(self):
        if BeautifulSoup:
            try:
                return BeautifulSoup(self.response.text, "lxml")
            except FeatureNotFound:
                info = ('please install lxml.\n'
                        'you can do: pip install lxml')

                raise ScraperError(info)

        else:
            info = ("please install beautiful soup\n"
                    "pip install beautifulsoup\n"
                    "pip install lxml\n")
            raise ScraperError(info)

    @property
    def tree(self):
        if html:
            return html.fromstring(self.response.content)
        else:
            info = ("please install lxml:\n"
                    "pip install lxml\n")
            raise ScraperError(info)

    def find_element_by_css_selector(self, css_selector):
        """
        Finds an element by css selector. This will only compute the first match
        and then it will return

        :Args:
         - css_selector: The css selector to use when finding elements.

        :Usage:
            driver.find_element_by_css_selector('#foo')
        """
        for s in self.soup.select(css_selector):
            return s

    def find_elements_by_css_selector(self, css_selector):
        """
        Finds elements by css selector.

        :Args:
         - css_selector: The css selector to use when finding elements.

        :Usage:
            driver.find_elements_by_css_selector('.foo')
        """
        return self.soup.select(css_selector)
