class ScraperError(Exception):
    '''
    Custom exception for the Scraper class.
    This should ideally help in hiding the implementation details i.e. not break
    excapsulation while provide as much details as possible to the user to help
    him correct the error in his code

    Parameters
    ----------
    message : str
        the message that is passed to the error and should be displayed

    Returns
    -------
    str
        should return the error message that is passed
    '''
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return "[ERROR] %s\n" % str(self.message)

    def log(self):
        ret = "%s" % str(self.message)
        if(hasattr(self, "reason")):
            return "".join([ret, "\n==> %s" % str(self.reason)])
        return ret
